### Unreleased

###### Added
 - Place search
 - Place Details
 - Tag Listing
 - Tag Creation

### 0.1.0-alpha - 2015-11-05
###### Added
###### Changed
###### Fixed
###### Removed
 - This CHANGELOG file to hopefully serve as an evolving example of a standardized open source project CHANGELOG.
 - CNAME file to enable GitHub Pages custom domain
 - README now contains answers to common questions about CHANGELOGs
 - Good examples and basic guidelines, including proper date formatting.
 - Counter-examples: "What makes unicorns cry?"
