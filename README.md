# Stackules

Hercules admin panel for Stackular

Uses generator-react-webpack as its Base

`https://github.com/newtriks/generator-react-webpack`

### Commands available for this project

```

# Start for development
npm start # or
npm run serve

# Start the dev-server with the dist version
npm run serve:dist

# Just build the dist version and copy static files
npm run dist

# Run unit tests
npm test

# Lint all files in src (also automatically done AFTER tests are run)
npm run lint

# Clean up the dist directory
npm run clean

# Just copy the static assets
npm run copy
```

### Generate new component

```
yo react-webpack:component my/namespaced/components/name
```

### Running tests

```
npm test or node node_modules/.bin/mocha
```
