'use strict';
import React from 'react';

class Base extends React.Component {
  /**
   * @param {String} methods method string arguments separated by comma
   * @returns null
   **/
  _bind(...methods) {
    methods.forEach((method) => this[method] = this[method].bind(this));
  }
}

export default Base;
