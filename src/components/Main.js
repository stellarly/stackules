require('normalize.css');
require('styles/App.css');

import React from 'react';

import Base from './Base';
import SearchPlace from './components/SearchPlaceComponent';
import Results from './components/SearchResultsComponent';
import Map from './components/MapComponent';
import PlaceDetails from './components/PlaceDetailsComponent';
import TagList from './components/TagListComponent';
import TagCreator from './components/TagCreatorComponent';

// let yeomanImage = require('../images/yeoman.png');

class AppComponent extends Base {

  constructor(){
    super();
    this.state = {
      items: [],
      place: {}
    };
    this._bind('assignResults', 'assignPlaceForDetails');
  }

  assignResults(res) {
    this.setState({
      items: res
    });
  }

  assignPlaceForDetails(place) {
    this.setState({
      place: place
    });
  }

  render() {
    return (
      <div className="index">
        <Map />
        <div className="placePanel">
          <h1>Place Finder</h1>
        <SearchPlace assignResults={this.assignResults} />
        <Results results={this.state.items} assignPlace={this.assignPlaceForDetails}/>
        <PlaceDetails place={this.state.place}></PlaceDetails>
        </div>

        <div className="tagPanel">
          <h1>Tag finder and editor</h1>
          <TagCreator></TagCreator>
          <TagList></TagList>
        </div>
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
