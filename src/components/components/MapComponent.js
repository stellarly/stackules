'use strict';

import React from 'react';
import Base from '../Base';

require('styles/components/Map.sass');

class MapComponent extends Base {

  constructor() {
    super();
    // this._bind('onChange', 'handleSubmit');
  }

  render() {
    return (
      <div id="map"></div>
    );
  }
}

MapComponent.displayName = 'ComponentsMapComponent';

// Uncomment properties you need
// SearchPlaceComponent.propTypes = {};
// SearchPlaceComponent.defaultProps = {};

export default MapComponent;
