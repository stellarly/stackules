'use strict';

import React from 'react';
import Base from '../Base';

require('styles/components/PlaceDetails.sass');

class PlaceDetailsComponent extends Base {

  constructor() {
    super();
    // this._bind('onChange', 'handleSubmit');
  }

  render() {

    if(!Object.keys(this.props.place).length) {
      return (
        <div>
          <p>Place Details will go here after clicking on a place id</p>
        </div>
      );
    } else {

      let place = this.props.place;
      let list = function(propName, index) {
        return(
          <li key={index + propName}>{propName}</li>
          )
      };

      let listPhotos = function(photoItem, index) {
        return(
          <li key={photoItem + index}>
            <img width="400" src={photoItem.getUrl({maxWidth: photoItem.width, maxHeight: photoItem.height})} />

          </li>
        )
      };

      return(
        <div>
          <h3>Place Details</h3>
          <p>
            International phone: {place.international_phone_number}
          </p>
          <p>
            Formatted phone: {place.formatted_phone_number}
          </p>
          <p>
            Price Level: {place.price_level}
          </p>
          <p>
            Rating: {place.rating}
          </p>
          <p>
            Google Maps Url: {place.url}
          </p>
            <p>Place Types</p>
            <ul>{ place.types ? place.types.map(list) : "None" }</ul>
            <p>Hours:</p>
            <ul>{ place.opening_hours ? place.opening_hours.weekday_text.map(list) : "Not Available" }</ul>

          <p>Website: {place.website}</p>
          <p>User ratings: {place.user_ratings_total}</p>

          <h5>Reviews:</h5>
          <p>can get them. wont for now</p>
          <h5>Photos:</h5>
          <ul>
            {place.photos ? place.photos.map(listPhotos) : "No photos available"}
          </ul>


        </div>
      );
    }


  }
}

//

PlaceDetailsComponent.displayName = 'ComponentsPlaceDetailsComponent';

// Uncomment properties you need
// SearchPlaceComponent.propTypes = {};
// SearchPlaceComponent.defaultProps = {};

export default PlaceDetailsComponent;
