'use strict';

import React from 'react';
import Base from '../Base';

import { findPlaceByName } from '../../services/google-places';
require('styles/components/SearchPlace.sass');

class SearchPlaceComponent extends Base {

  constructor() {
    super();
    this.state = {
      text: ''
    };
    this._bind('onChange', 'handleSubmit');
  }

  onChange(e) {
    this.setState({
      text: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    findPlaceByName(this.state.text, callback.bind(this));
    function callback(results, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        this.props.assignResults(results);
      }
    }
  }

  render() {
    return (
        <div className="searchplace-component">
          <form onSubmit={this.handleSubmit}>
            <p>Search a Place:</p>
            <input onChange={this.onChange} value={this.state.text}/>
          </form>
        </div>
    );
  }
}

SearchPlaceComponent.displayName = 'ComponentsSearchPlaceComponent';

// Uncomment properties you need
// SearchPlaceComponent.propTypes = {};
// SearchPlaceComponent.defaultProps = {};

export default SearchPlaceComponent;
