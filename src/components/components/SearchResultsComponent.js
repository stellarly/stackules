'use strict';

import React from 'react';
import Base from '../Base';

require('styles/components/SearchResults.sass');

import { findPlaceInfo } from '../../services/google-places';

class SearchResultsComponent extends Base {

/**
*  @prop results
*/
  constructor() {
    super();
    this.state = {
      placeDetails : {}
    };
    // this._bind('render');
  }

  getMoreInfo(placeid) {
    console.log('finding more info for this place id', placeid);
    function callback(place, status) {
      this.props.assignPlace(place);
      console.log('found place', place);
    }
    findPlaceInfo(placeid, callback.bind(this));
  }

  render() {
    console.log('this.props.results', this.props.results);

    var context = this;
    var createItem = function(place, index) {
      return (
        <li key={index + place.place_id}>
          <ul className="place-profile-container">
            <li>
              <div className="clearfix">
                <img className="left place-icon" src={place.icon} />
                <p className="left">{place.name}</p>
              </div>
            </li>
            <li>
              <a onClick={context.getMoreInfo.bind(context, place.place_id)}>Place id: {place.place_id}</a>
            </li>
            <li>
              <p>Address: {place.formatted_address}</p>
            </li>
          </ul>
        </li>
      );
    };

    return (
      <div className="searchresults-component">
        <p>Place Query Results: </p>
        <ul>{this.props.results.map(createItem)}</ul>
      </div>
    );
  }

}

export default SearchResultsComponent;
