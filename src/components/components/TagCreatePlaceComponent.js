'use strict';

import React from 'react';

const Firebase = require('firebase');

export default React.createClass({
  getInitialState() {
    this.ref = new Firebase('https://stackular.firebaseio.com/');

    return {
      placeText: ''
    };
  },

  onSubmit(e) {
    e.preventDefault();

    // store in firebase by gmaps id instead of firebse id?

    // TODO: check if the place already exists
    let places = this.ref.child('places');

    // TODO: if place doesn't exit, create place by pushing,
    // TODO: add gplaces name
    // TODO: add gplaces_id to it

    // TODO: Now check if the tag had been added to places

    // TODO: if tag wasnt added, then add it to the place

    //TODO: place exists:
    //TODO: check it place is already in tags
    //TODO: if place wasnt added, then add that place to the tag

    let tag = this.ref.child('tags').child(this.props.tag['.key']);

    // let place = this.ref.child('places')

    let tempObj = {};
    tempObj['gplaces_id'] = this.state.placeText;
    let tag_places = tag.child('places');

    if(!tag_places) {
      tag.update({
        places: tempObj
      })
    } else{
      tag_places.push().set(tempObj);
    }
    this.setState({
      placeText: ''
    });
    this.props.doneAddingPlace();
  },

  onChange(e) {
    e.preventDefault();
    this.setState({
      placeText : e.target.value
    });
  },

  render() {
      return (
        <div className="place-creator-component">
          <form className="form-create-place" onSubmit={this.onSubmit}><input value={this.state.placeText} onChange={this.onChange}/></form>
        </div>
      );
  }
});
