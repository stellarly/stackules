'use strict';

import React from 'react';

const Firebase = require('firebase');
// const ReactFireMixin = require('reactfire');

require('styles/components/TagCreator.sass');

export default React.createClass({

  // mixins: [ReactFireMixin],

  getInitialState() {

    this.ref =  new Firebase('https://stackular.firebaseio.com/');

    return {
      text: '',
    };
  },


  onChange(e) {
    this.setState({
      text: e.target.value
    });
  },

  handleSubmit(e) {
    e.preventDefault();

    if(this.state.text.length < 3) {
      console.log('add a longer tag name')
      return;
    }

    let tags = this.ref.child('tags');

    tags.push().set({
      name: this.state.text,
      modified_ts: +(new Date()),
      places: [],
      iconUri: 'sta-icon-'+this.state.text.replace(' ', '-').toLowerCase()
    });

    console.log('submitting', e);
    e.preventDefault();

  },

  // componentWillMount() {
  //   this.ref = new Firebase('https://stackular.firebaseio.com/tags/');
  //   this.bindAsArray(ref, "tags");
  // },

  render() {

    return (
        <div className="tagcreator-component">
          <p>Create a Tag:</p>
          <form onSubmit={this.handleSubmit}>
          <input onChange={this.onChange} value={this.state.text}/>
          </form>
        </div>
    );
  }
});
