'use strict';

import React from 'react';

require('styles/components/TagIcon.sass');

const Firebase = require('firebase');

// import Base from '../Base';

export default React.createClass({
  getInitialState() {
    this.tags = new Firebase('https://stackular.firebaseio.com/tags/');

    return {
      editing: false,
      iconUri: ''
    };
  },

  onClick(e) {
    e.preventDefault();
    this.setState({
      editing: true,
      iconUri: this.props.iconUri
    });
  },

  onChange(e) {
    e.preventDefault();
    this.setState({
      iconUri: e.target.value
    });
  },

  onSubmit(e){
    e.preventDefault();

    if(this.props.iconUri !== this.state.iconUri) {
      let tag = this.tags.child(this.props.id);
      tag.update({
        'iconUri': this.state.iconUri
      });
    }
    this.setState({
      editing: false
    });
  },

  render() {
    // if () {
      return (
        <div className="tagicon-component">
        IconUri: &nbsp;
        {this.state.editing ? <form className="tag-icon-form" onSubmit={this.onSubmit}> <input value={this.state.iconUri} onChange={this.onChange}/> </form> : <span onClick={this.onClick}>{this.props.iconUri}</span>}
        </div>
      );
  }
});
