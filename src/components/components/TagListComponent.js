'use strict';

import React from 'react';

const Firebase = require('firebase');
const ReactFireMixin = require('reactfire');

import TagName from './TagNameComponent';
import TagIcon from './TagIconComponent';
import TagPlaces from './TagPlacesComponent';



require('styles/components/TagList.sass');

export default React.createClass({

  mixins: [ReactFireMixin],

  getInitialState() {
    return {
      tags: []
    };
  },

  componentWillMount() {
    let ref = new Firebase('https://stackular.firebaseio.com/tags/');
    this.bindAsArray(ref, "tags");
  },

  listTags(obj, index) {
    return (
      <li key={index} className="tag-list-item">
        <TagName name={this.state.tags[index].name} id={this.state.tags[index]['.key']}></TagName>
        <p> id: {obj['.key']} </p>
        <TagPlaces tag={obj}></TagPlaces>
        <TagIcon iconUri={obj.iconUri} id={obj['.key']}></TagIcon>
      </li>
    )
  },

  render() {
    return (
      <div className="taglist-component">
        <p>Available Tags:</p>
        <ul>{this.state.tags.map(this.listTags)}</ul>
      </div>
    );
  }
});
