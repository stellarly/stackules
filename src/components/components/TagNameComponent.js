'use strict';

import React from 'react';

require('styles/components/TagName.sass');

const Firebase = require('firebase');

// import Base from '../Base';

export default React.createClass({
  getInitialState() {
    this.tags = new Firebase('https://stackular.firebaseio.com/tags/');

    return {
      editing: false,
      name: ''
    };
  },

  onClick(e) {
    e.preventDefault();
    this.setState({
      editing: true,
      name: this.props.name
    });
  },

  onChange(e) {
    e.preventDefault();
    this.setState({
      name: e.target.value
    });
  },

  onSubmit(e){
    e.preventDefault();

    if(this.props.name !== this.state.name) {
      let tag = this.tags.child(this.props.id);
      tag.update({
        'name': this.state.name
      });
    }
    this.setState({
      editing: false
    });
  },

  render() {
    if (this.state.editing) {
      return (
        <div className="tagname-component">
          <form onSubmit={this.onSubmit}>
          <input value={this.state.name} onChange={this.onChange}/>
          </form>
        </div>
      )
    }
      return (
        <div className="tagname-component">
          <p onClick={this.onClick}>
            Name: {this.props.name}
          </p>
        </div>
      );
  }
});
