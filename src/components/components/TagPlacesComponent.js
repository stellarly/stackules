'use strict';
import React from 'react';
require('styles/components/TagPlaces.sass');

const Firebase = require('firebase');

import CreatePlaceComponent from './TagCreatePlaceComponent';

export default React.createClass({
  getInitialState() {
    // this.tags = new Firebase('https://stackular.firebaseio.com/tags/');

    return {
      addPlaceIcon: false,
      addPlace: false
    };
  },

  toggleEditIcon() {
    this.setState({
      addPlaceIcon: !this.state.addPlaceIcon
    });
  },

  listPlaces(places) {
    return Object
      .keys(places)
      .map(function(placeKey) {
        return (
          <li key={placeKey}>{places[placeKey].gplaces_id}</li>
        );
      });
  },

  togglePlaceCreator() {
    this.setState({
      addPlace: !this.state.addPlace
    });
  },

  render() {
      return (
        <div className="tagplaces-component">
        <h3 className="places-list-title" onClick={this.togglePlaceCreator} onMouseOver={this.toggleEditIcon} onMouseLeave={this.toggleEditIcon}>Places in tag</h3>
        <span className="plus-sign">{this.state.addPlaceIcon ? '+' : ''}</span>

        {this.state.addPlace ? <CreatePlaceComponent doneAddingPlace={this.togglePlaceCreator} tag={this.props.tag}></CreatePlaceComponent> : <span></span>}

        <ul>
          {typeof this.props.tag.places === 'object' ? this.listPlaces(this.props.tag.places) : <li key={JSON.stringify(this.props.tag)}><p>No Places</p></li> }
        </ul>
        </div>
      );
  }
});
