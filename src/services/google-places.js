const $ = require('jquery');

export function findPlaceByName(name, callback) {

  let request = {
    location: miami,
    radius: '500',
    query: name
  };

  let service = new google.maps.places.PlacesService(map);
  service.textSearch(request, callback);

}

export function findPlaceInfo(placeid, callback) {

  let service = new google.maps.places.PlacesService(map);
  service.getDetails({
      placeId: placeid //'ChIJsRbg74S22YgRi0Phf5bJduk'
    }, callback);
}

// let google = google || null;
//
// if(!google) {
//   google = {
//     maps: {
//       Map: function(){},
//       LatLng: function(){},
//       places: {
//         PlacesServiceStatus: {
//           OK: "OK"
//         }
//       }
//     }
//   };
// }
